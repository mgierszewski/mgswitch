//
//  Switch.m
//
//  Created by Maciej Gierszewski on 27.11.2012.
//  Copyright (c) 2012 Future Mind sp. z o. o. All rights reserved.
//

#import "MGSwitch.h"
#import <QuartzCore/QuartzCore.h>

CGFloat const DEFAULT_WIDTH = 51.0f;
CGFloat const DEFAULT_HEIGHT = 31.0f;

UIEdgeInsets const BORDER = {1.5f, 1.5f, 1.5f, 1.5f};

typedef NS_ENUM(NSUInteger, GestureState)
{
    GestureStateNone,
    GestureStatePressed,
    GestureStateMoved,
    GestureStateCancelled
};

@interface MGSwitch ()
@property (nonatomic, strong) UIImageView* thumbView;
@property (nonatomic, strong) UIImageView* onView;
@property (nonatomic, strong) UIImageView* offView;
@property (nonatomic, strong) UIImageView* backgroundView;

@property (nonatomic, readonly) CGSize thumbSize;
@property (nonatomic, readonly) UIEdgeInsets shadowInset;

- (void)longPressGestureRecognized:(UILongPressGestureRecognizer*)recognizer;

- (void)setup;

- (UIImage*)createImageFromRectWithSize:(CGSize)size inset:(UIEdgeInsets)inset color:(UIColor*)color shadow:(BOOL)shadow;
@end

@implementation MGSwitch
{
    GestureState _gestureState;
}

- (id)init
{
    self = [super initWithFrame:CGRectMake(0, 0, DEFAULT_WIDTH, DEFAULT_HEIGHT)];
    if (self)
	{
		[self setup];
	}
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if( self )
    {
        [self setup];
    }
    return self;
}

- (void)setup
{
    self.backgroundColor = [UIColor clearColor];
    
    _on = NO;
    _gestureState = GestureStateNone;
    
    self.backgroundView = [[UIImageView alloc] initWithFrame:self.bounds];
	self.backgroundView.contentMode = UIViewContentModeCenter;
	[self addSubview:self.backgroundView];
	
    self.onView = [[UIImageView alloc] initWithFrame:self.bounds];
    self.onView.contentMode = UIViewContentModeCenter;
	[self addSubview:_onView];
	
    self.offView = [[UIImageView alloc] initWithFrame:self.bounds];
	self.offView.contentMode = UIViewContentModeCenter;
	[self addSubview:self.offView];
	
	self.thumbView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.thumbSize.width, self.thumbSize.height)];
    self.thumbView.contentMode = UIViewContentModeScaleToFill;
	[self addSubview:self.thumbView];
    
    _borderTintColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    _backgroundView.image = [self createImageFromRectWithSize:_backgroundView.frame.size
                                                    inset:UIEdgeInsetsZero
                                                    color:_borderTintColor
                                                   shadow:NO];
    
    _onTintColor = [UIColor redColor];
    _onView.image = [self createImageFromRectWithSize:_onView.frame.size
                                                inset:UIEdgeInsetsZero
                                                color:_onTintColor
                                               shadow:NO];
    
    _offTintColor = [UIColor colorWithWhite:1.0f alpha:1.0f];
    _offView.image = [self createImageFromRectWithSize:_offView.frame.size
                                                 inset:BORDER
                                                 color:_offTintColor
                                                shadow:NO];
    
    _thumbTintColor = [UIColor colorWithWhite:1.0f alpha:1.0f];
    _thumbView.image = [self createImageFromRectWithSize:self.thumbSize
                                                   inset:BORDER
                                                   color:_thumbTintColor
                                                  shadow:YES];
    
    UILongPressGestureRecognizer* longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressGestureRecognized:)];
    longPressGestureRecognizer.minimumPressDuration = 0.0f;
    [self addGestureRecognizer:longPressGestureRecognizer];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    BOOL onViewVisible = NO;
    BOOL offViewVisible = NO;
    
    CGSize thumbSize = self.thumbSize;
    CGPoint thumbPosition = CGPointZero;
    
    if( _gestureState == GestureStateNone )
    {
        thumbSize.width = self.thumbSize.width;
        
        offViewVisible = !_on;
        onViewVisible = _on;
    }
    else
    {
        thumbSize.width = self.thumbSize.width * 1.25;
       
        offViewVisible = NO;
        onViewVisible = (_gestureState == GestureStateMoved) ^ _on;
    }
    
    thumbPosition.x = onViewVisible ? CGRectGetWidth(self.frame) - thumbSize.width : 0.0f;
    
    CGRect thumbFrame = _thumbView.frame;
    thumbFrame.size.width   = thumbSize.width + self.shadowInset.left + self.shadowInset.right;
    thumbFrame.size.height  = thumbSize.height + self.shadowInset.top + self.shadowInset.bottom;
    thumbFrame.origin.x     = thumbPosition.x - self.shadowInset.left;
    thumbFrame.origin.y     = thumbPosition.y - self.shadowInset.top;
    
    _thumbView.frame = thumbFrame;
    _offView.transform = CGAffineTransformMakeScale(offViewVisible, offViewVisible);
    _onView.alpha = onViewVisible;
}

#pragma mark - Getters

- (CGSize)thumbSize
{
    return CGSizeMake( CGRectGetHeight(self.frame), CGRectGetHeight(self.frame) );
}

- (UIEdgeInsets)shadowInset
{
    return UIEdgeInsetsMake(CGRectGetHeight(self.frame)/2.0f,
                            CGRectGetHeight(self.frame)/2.0f,
                            CGRectGetHeight(self.frame)/2.0f,
                            CGRectGetHeight(self.frame)/2.0f);
}

#pragma mark - Setters

- (void)setEnabled:(BOOL)enabled
{
    [super setEnabled:enabled];
    
    if( enabled )
    {
        _thumbView.alpha = 1.0;
        _offView.alpha = 1.0;
        _onView.alpha = 1.0;
    }
    else
    {
        if( _on )
        {
            _onView.alpha = 0.3f;
            _offView.alpha = 0.0;
        }
        else
        {
            _onView.alpha = 0.0;
            _offView.alpha = 0.3f;
        }
    }
}

- (void)setOn:(BOOL)value animated:(BOOL)animated
{
    self.on = value;
    
    [self setNeedsLayout];
    [UIView animateWithDuration:0.25
                          delay:0.0f
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [self layoutIfNeeded];
                     }
                     completion:^(BOOL finished) {
                     }];
}

#pragma mark - UIGestureRecognizers

- (void)longPressGestureRecognized:(UILongPressGestureRecognizer *)recognizer
{
    BOOL valueChanged = NO;
    
    if( recognizer.state == UIGestureRecognizerStateBegan  )
    {
        _gestureState = GestureStatePressed;
    }
    else if( recognizer.state == UIGestureRecognizerStateChanged )
    {
        CGPoint touchLocation = [recognizer locationInView:self];
        if( _on && touchLocation.x < CGRectGetWidth(self.frame) * 0.3f )
        {
            _gestureState = GestureStateMoved;
        }
        else if( !_on && touchLocation.x > CGRectGetWidth(self.frame) * 0.7f )
        {
            _gestureState = GestureStateMoved;
        }
        else if( _gestureState == GestureStateMoved )
        {
            _gestureState = GestureStateCancelled;
        }
    }
    else if( recognizer.state == UIGestureRecognizerStateEnded )
    {
        CGPoint touchLocation = [recognizer locationInView:self];
        if( CGRectContainsPoint(self.bounds, touchLocation) && _gestureState == GestureStatePressed )
        {
            _on = !_on;
            valueChanged = YES;
        }
        else if( _gestureState == GestureStateMoved )
        {
            _on = !_on;
            valueChanged = YES;
        }
        _gestureState = GestureStateNone;
    }
    
    [self setNeedsLayout];
    [UIView animateWithDuration:0.25
                          delay:0.0f
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [self layoutIfNeeded];
                     }
                     completion:^(BOOL finished) {
                         
                         if( valueChanged )
                         {
                             [self sendActionsForControlEvents:UIControlEventValueChanged];
                         }
                     }];
}


#pragma mark - UIAppearance

- (void)setOnTintColor:(UIColor *)onTintColor
{
    _onTintColor = onTintColor;
    _onView.image = [self createImageFromRectWithSize:_onView.bounds.size
                                                inset:UIEdgeInsetsZero
                                                color:_onTintColor
                                               shadow:NO];
}

- (void)setOffTintColor:(UIColor *)offTintColor
{
    _offTintColor = offTintColor;
    _offView.image = [self createImageFromRectWithSize:_offView.bounds.size
                                                 inset:BORDER
                                                 color:_offTintColor
                                                shadow:NO];
}

- (void)setBorderTintColor:(UIColor *)borderTintColor
{
    _borderTintColor = borderTintColor;
    _backgroundView.image = [self createImageFromRectWithSize:_backgroundView.bounds.size
                                                    inset:UIEdgeInsetsZero
                                                    color:_borderTintColor
                                                   shadow:NO];
}

- (void)setThumbTintColor:(UIColor *)thumbTintColor
{
    _thumbTintColor = thumbTintColor;
    _thumbView.image = [self createImageFromRectWithSize:self.thumbSize
                                                   inset:BORDER
                                                   color:_thumbTintColor
                                                  shadow:YES];
}

- (UIImage*)createImageFromRectWithSize:(CGSize)size inset:(UIEdgeInsets)inset color:(UIColor*)color shadow:(BOOL)shadow
{
    CALayer* layer = [CALayer layer];
    layer.frame = CGRectMake(0.0f, 0.0f, size.width - inset.left - inset.right, size.height - inset.top - inset.bottom);
    layer.cornerRadius = MIN( size.width - inset.left - inset.right, size.height - inset.top - inset.bottom ) / 2.0f;
    layer.backgroundColor = color.CGColor;
    
    if( shadow )
    {
        layer.shadowOpacity = 0.5f;
        layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
        layer.shadowColor = [[UIColor blackColor] CGColor];
        layer.shadowRadius = 1.5;
        
        UIGraphicsBeginImageContextWithOptions( CGSizeMake(size.width + self.shadowInset.left + self.shadowInset.right, size.height + self.shadowInset.top + self.shadowInset.bottom), NO, 0.0f);
        CGContextTranslateCTM(UIGraphicsGetCurrentContext(), self.shadowInset.left + inset.left, self.shadowInset.top + inset.top);
    }
    else
    {
        UIGraphicsBeginImageContextWithOptions(size, NO, 0.0f);
        CGContextTranslateCTM(UIGraphicsGetCurrentContext(), inset.left, inset.top);
    }
    
    [layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage* image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return [image resizableImageWithCapInsets:UIEdgeInsetsMake(image.size.height/2.0f, image.size.width/2.0f, image.size.height/2.0f, image.size.width/2.0f)];
}

@end