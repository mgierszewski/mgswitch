//
//  MGSwitch.h
//
//  Created by Maciej Gierszewski on 27.11.2012.
//  Copyright (c) 2012 Future Mind sp. z o. o. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MGSwitch : UIControl
@property (nonatomic, strong) UIColor* onTintColor          UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor* offTintColor         UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor* thumbTintColor       UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor* borderTintColor      UI_APPEARANCE_SELECTOR;

@property (nonatomic, getter = isOn) BOOL on;
- (void)setOn:(BOOL)on animated:(BOOL)animated;

@end
