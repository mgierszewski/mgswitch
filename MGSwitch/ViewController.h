//
//  ViewController.h
//  SwitchApp
//
//  Created by Maciej Gierszewski on 04.02.2014.
//  Copyright (c) 2014 Future Mind sp. z o. o. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@end
