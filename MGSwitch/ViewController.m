//
//  ViewController.m
//  SwitchApp
//
//  Created by Maciej Gierszewski on 04.02.2014.
//  Copyright (c) 2014 Future Mind sp. z o. o. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
